# Infoconcert API

A scrapper for the [infoconcert](https://www.infoconcert.com/) website, that I made because I couldn't find any satisfying concerts API.

## Installation

```sh
npm install git+https://gitlab.com/Emeraude/infoconcert-api.git
```

## Usage

```js
const { searchArtists, getConcertsByArtist } = require('infoconcert-api')

async function example(artistName) {
  const artists = await searchArtists(artistName)
  console.log(artists)
  const concerts = await getConcertsByArtist(artists[0])
  console.log(concerts)
}

example("Alestorm")
```

const fetch = require("node-fetch")
const cheerio = require("cheerio")
const currencies = require('currency-symbol-map/map')

const DOMAIN = "https://www.infoconcert.com"

async function searchArtists(query) {
  const req = await fetch(DOMAIN + '/recherche-concert.html?search_type=artiste&motclef='
                          + encodeURIComponent(query))
  const body = await req.text()
  const $ = cheerio.load(body)
  return $('.search-results-cat > .bg_content_search > .results-line > a')
    .toArray()
    .map((e) => {
      return {
        "name": e.children[0].data.trim(),
        "genre": e.children[1].children[0].data.slice(1, -1),
        "url": DOMAIN + e.attribs['href']
      }
    })
}

function parseDate($, e) {
  const months = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"]
  const time = $('.hour', $(e)).text().split('h')
  return {
    "datetime": new Date($('.year', $(e)).text(),
                         months.indexOf($('.month', $(e)).text().toLowerCase()),
                         $('.day', $(e)).text(),
                         time[0],
                         time[1])
  }
}

function parseLocation($, e) {
  return {
    "location": {
      "room": {
        "name": $('.salle > a', $(e)).text().trim(),
        "url": DOMAIN + $('.salle > a', $(e)).attr('href'),
      },
      "city": {
        "name": $('.ville-dpt > a', $(e)).text().trim(),
        "url": DOMAIN + $('.ville-dpt > a', $(e)).attr('href'),
        "department": parseInt($('.ville-dpt', $(e)).toArray()[0].children[2].data.trim().slice(1, -1)) || null,
      }
    }
  }
}

function parseArtists($, e) {
  return {
    "artists": $('.spectacle > a', $(e))
      .toArray()
      .map(e => {
        return {
          "name": e.children[0].data.trim(),
          "url": DOMAIN + e.attribs['href']
        }
      }),
  }
}

const customCurrencies = {
  "FS": "CHF"
}

function currencySymbolToName(sym) {
  return customCurrencies[sym] || Object.entries(currencies).find(([_, c]) => c === sym)[0] || null
}

const statuses = {
  "RÉSERVEZ VITE !": "reservable",
  "REPORTÉ": "postponed",
  "ALERTE RÉSERVATION": "alert",
  "ANNULÉ": "canceled",
  "PLUS RÉSERVABLE": "unreservable",
  "PLUS RESERVABLE": "unreservable",
  "COMPLET": "soldout",
  "": "unknown"
}

function parsePrice($, e) {
  const ret = {
    price: null,
    "status": "unknown"
  } // "status" => ["canceled", "postponed", "reservable", "soldout", "unreservable", "alert", "unknown"], "price" => {"min", "max", "currency"}, "reservation_url"
  const price = $('.price', $(e)).text().trim()
  const PRICE_MATCH = "\\d+(?:.\\d{1,2})?"
  const FIXED_PRICE_MATCH = "^((" + PRICE_MATCH + "))\\s*(.*)$"
  const RANGED_PRICE_MATCH = "^de\\s(" + PRICE_MATCH + ")\\s*à\\s(" + PRICE_MATCH + ")(.*)$"
  const match = price.match(FIXED_PRICE_MATCH)
        || price.match(RANGED_PRICE_MATCH)
  if (match) {
    ret.price = {
      min: parseFloat(match[1]),
      max: parseFloat(match[2]),
      currency: currencySymbolToName(match[3])
    }
  }
  ret.status = statuses[price] || statuses[$('.text-right > .btn', $(e)).text().trim()] || "unknown"
  ret.reservation_url = $('.text-right > a.btn_reservez', $(e)).attr('href') || null
  return ret
}

async function getConcertsByArtist(artist) {
  const req = await fetch(artist.url)
  const body = await req.text()
  const $ = cheerio.load(body)
  return $('.date-line > .panel-body > .row:not(.actions)')
    .toArray()
    .map(e => {
      return Object.assign(
        parseDate($, $('.date', $(e))),
        parseLocation($, $('.location', $(e))),
        parseArtists($, $('.spectacle', $(e))),
        parsePrice($, $('.concert-price-url', $(e))),
        // { "url": DOMAIN + $('.row.actions .more-infos > a', $(e.parent)).attr('href') },
      )
    })
}

module.exports = { searchArtists, getConcertsByArtist }
